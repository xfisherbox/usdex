USDEX

1. Кошелек-ноду загружаем с https://github.com/owner232/USDEX/releases/
Запускать нужно с ключами для RPC-протокола, например:

USDEx-qt2.exe -server -rpcuser=a -rpcpassword=b -rpcport=14022 -rpcallowip=127.0.0.1

2. Функции и описание

 2.1 Уникальный адрес кошелька создается
 2.2 Реализован метод получения баланса общего кошелька
 2.3 Реализован метод получения приходного баланса конечного (по заданному адресу) кошелька
 2.4 Минимальная коммисия которая была выявлена во время тестов 0.0001 USDEX
 2.5 Безопасность ноды решается ключами  -rpcuser=a -rpcpassword=b -rpcport=14022 -rpcallowip=127.0.0.1
 2.6 Кол-во подтвержденных транзакций - 20
 2.7 Монеты отправляются с общего (основного) кошелька.
