package main

import "testing"


var USDEX = NewClient("http://a:b@localhost:14022/", 20);


// 0.0001 USDEX min

func TestSetFee(t *testing.T) {

	resp, err := USDEX.SetFee(0.0001)
  if err != nil {
     t.Errorf("setFee error: %+v", err)
     t.FailNow()
  }
  t.Logf("setFee result: %v", resp)
}

func TestCreateAddress(t *testing.T) {

	resp, err := USDEX.CreateAddress()
	if err != nil {
		 t.Errorf("createAddress error: %+v", err)
		 t.FailNow()
	}
	t.Logf("createAddress result: %v", resp)
}

func TestGetBalance(t *testing.T) {

  resp, err := USDEX.GetBalance()
  if err != nil {
     t.Errorf("getBalance error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalance result: %v", resp)
}

func TestGetWalletInfo(t *testing.T) {
  resp, err := USDEX.GetWalletInfo()
  if err != nil {
     t.Errorf("getWalletInfo error: %+v", err)
     t.FailNow()
  }
  t.Logf("getWalletInfo result: %+v", resp)
}

func TestGetBalanceByAddress(t *testing.T) {
  resp, err := USDEX.GetBalanceByAddress("8a3Pdy4RdSSJjDNkPAEfE6moWcD8w9d28o")
  if err != nil {
     t.Errorf("getBalanceByAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalanceByAddress result: %v", resp)
}


func TestSendToAddress(t *testing.T) {

  addr := "8QxxUtxpiJXuGybxkLGLBKHyopfR9U8GpQ"
  resp, err := USDEX.SendToAddress(addr, 0.1)
  if err != nil {
     t.Errorf("sendToAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("sendToAddress result: %v", resp)
}



func TestGetTransaction(t *testing.T) {
  resp, err := USDEX.GetTransaction("d942650ee39d012e54cf80b8c1fa92c8a0d5e31022f97c5e4e6b1c6a03709519")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}

func TestCheckTransaction(t *testing.T) {
  resp, err := USDEX.CheckTransaction("3e7dfecc1a799d2b6d83ca7ecac9f1036dd73745a2ad4885b128d37022960bc9")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}
